<?php

namespace AnowarCST\PrometheusLaravelApiHealth;

use AnowarCST\PrometheusLaravelApiHealth\Middleware\LaravelResponseTimeMiddleware;
use Illuminate\Foundation\Http\Kernel;
use Illuminate\Support\ServiceProvider;

class LaravelRequestMonitoringServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->make('AnowarCST\PrometheusLaravelApiHealth\Controllers\PrometheusController');

        $this->mergeConfigFrom(__DIR__ . '/config/config.php', 'prometheus_exporter');

        // $storageAdapter = $this->getConfigInstance('adapter');
        // $this->app->bind('Prometheus\Storage\Adapter', $storageAdapter);

        // $this->app->singleton(CollectorRegistry::class, function () use ($storageAdapter) {
        //     return new CollectorRegistry($storageAdapter);
        // });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot(Kernel $kernel)
    {
        $this->loadRoutesFrom(__DIR__ . '/routes.php');
        $source = realpath(__DIR__ . '/config/config.php');
        $this->publishes([$source => config_path('prometheus_exporter.php')]);

        $kernel->pushMiddleware(LaravelResponseTimeMiddleware::class);
    }
}
