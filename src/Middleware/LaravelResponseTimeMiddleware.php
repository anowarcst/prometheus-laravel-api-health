<?php

namespace AnowarCST\PrometheusLaravelApiHealth\Middleware;


class LaravelResponseTimeMiddleware extends AbstractResponseTimeMiddleware
{
    protected function getRouteNames(): array
    {
        $routeNames = [];
        foreach (\Route::getRoutes() as $route) {
            $routeNames[] = $route->getName() ?: $route->uri();
        }
        return $routeNames;
    }

    /**
     * Get route name
     *
     * @return string
     */
    protected function getRouteName(): string
    {
        return \Route::currentRouteName() ?: \Request::path();
    }
}
